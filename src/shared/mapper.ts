// import { TaskDto } from '@todo/dto/task.dto';
// import { TodoEntity } from '@todo/entity/todo.entity';
// import { TodoDto } from '@todo/dto/todo.dto';
// import { TaskEntity } from '@todo/entity/task.entity';
import { User } from '../users/user.entity';
import { UserDto } from '../users/dto/user.dto';

export const toUserDto = (data: User): UserDto => {
  const { id, username, email,roles } = data;

  let userDto: UserDto = {
    id,
    username,
    email,
    roles
  };

  return userDto;
};
