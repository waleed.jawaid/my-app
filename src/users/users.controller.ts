import {
  Controller,
  Post,
  Body,
  Get,
  Put,
  Delete,
  Param,
  UseInterceptors,
  UploadedFile,
  UseGuards,
  Req
} from '@nestjs/common';
import { AuthGuard, PassportModule } from '@nestjs/passport';
import { Roles } from '../roles/role.decorator';
import { Role } from '../roles/role.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from '../utils/file-upload.utils';
import { Request } from 'express';


@Controller('users')
export class UsersController {
  constructor(private service: UsersService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
 // @Roles(Role.User)
  getAllUsers(@Req() request: Request) {
    return this.service.getAllUsers();
  }
  @Get('/shops')
  getAllUsersWithShops() {
    return this.service.getAllUsersWithShops();
  }

  @Get('/getQuery')
  getQuery() {
    return this.service.getQuery();
  }

  // @Get(':id')
  // get(@Param() params) {
  //   return this.service.getUser(params.id);
  // }

  @Post('/upload')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async uploadedFile(@UploadedFile() file) {
    const response = {
      originalname: file.originalname,
      filename: file.filename,
    };
    return response;
  }

  @Put()
  update(@Body() user: User) {
    return this.service.updateUser(user);
  }

  @Delete(':id')
  deleteUser(@Param() params) {
    return this.service.deleteUser(params.id);
  }
}
