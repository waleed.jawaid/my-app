import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import { CreateUserDto } from './dto/user.create.dto';
import { LoginUserDto } from './dto/user.login.dto';
import { comparePasswords } from '../shared/utils';
import { toUserDto } from '../shared/mapper';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  async create(userDto: CreateUserDto): Promise<UserDto> {
    const { username, password, email } = userDto;
    // check if the user exists in the db
    const userInDb = await this.usersRepository.findOne({
      where: { username },
    });
    if (userInDb) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }
    const user: User = await this.usersRepository.create({
      username,
      password,
      email,
    });
    await this.usersRepository.save(user);
    return toUserDto(user);
  }

  async findByLogin({ username, password }: LoginUserDto) {
    const user = await this.usersRepository.findOne({ where: { username } });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
    }
    // compare passwords
    const areEqual = await comparePasswords(user.password, password);
    if (!areEqual) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
    return toUserDto(user);
  }

  async findByPayload({ username }: any): Promise<UserDto> {
    return await this.findOne({
      where: { username },
    });
  }

  async findOne(options?: object): Promise<UserDto> {
    const user = await this.usersRepository.findOne(options);
    return toUserDto(user);
  }

  async getAllUsers(): Promise<User[]> {
    return await this.usersRepository.find({select: ['username', 'email', 'isActive']});
  }

  async getQuery(): Promise<{}> {
    const user = await getRepository(User)
      .createQueryBuilder('user')
      .where('user.id = :id', { id: 4 })
      .getOne();
    return { user: user };
  }

  async getAllUsersWithShops(): Promise<User[]> {
    return await this.usersRepository.find({ relations: ['shops'] });
  }

  // async getUser(_id: number): Promise<User[]> {
  //   return await this.usersRepository.find({
  //     select: ['fullName', 'birthday', 'isActive'],
  //     where: [{ id: _id }],
  //   });
  // }

  async updateUser(user: User) {
    this.usersRepository.save(user);
  }

  async createUser(user: User) {
    this.usersRepository.save(user);
  }

  async deleteUser(user: User) {
    this.usersRepository.delete(user);
  }
}
