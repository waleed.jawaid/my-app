import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  BeforeInsert,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Shop } from '../shops/shop.entity';
import { Role } from '../roles/role.enum';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  username: string;

  @Column('date')
  birthday: Date;

  @Column()
  isActive: boolean;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @BeforeInsert() async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  @OneToMany(() => Shop, (shop) => shop.user)
  shops: Shop[];

  @Column({
    type: 'varchar',
    nullable: false,
  })
  roles: string;


}
